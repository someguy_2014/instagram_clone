<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    use HasFactory;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'description',
        'url',
        'image'
    ];
    
    function profileImage(){
        return '/storage/' . (($this->image) ? $this->image : 'profile/no_image_available.png');
    }


    function user(){
        return $this->belongsTo(User::class);
    }
    
    function followers() {
        return $this->belongsToMany(User::class);
    }
}
