<?php

namespace App\Http\Controllers;

use Intervention\Image\Facades\Image;
use \App\Models\Post;

class PostsController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }
    
    function index(){
        $users = auth()->user()->following()->pluck('profiles.user_id');
        $posts = Post::whereIn('user_id', $users)->with('user')->latest()->paginate(5);
        
        return view('posts.index', compact('posts'));
    }


    function create(){
        return view('posts.create');
    }

    function store(){
        $data = request()->validate([
            'caption' => 'required',
            'image'   => ['required', 'image'],
        ]);

        $imagePath = request('image')->store('uploads', 'public');
        $image = Image::make(public_path("storage/{$imagePath}"))->fit(1200, 1200);
        $image->save();

        auth()->user()->posts()->create([
            'caption' => $data['caption'],
            'image'   => $imagePath,
        ]);

        return redirect('/profile/' . auth()->user()->id);
    }

    function show(Post $post){        
        return view('posts.show', compact('post'));
    }
}
