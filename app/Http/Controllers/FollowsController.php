<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FollowsController extends Controller
{
    function store(\App\Models\User $user){
        
        return auth()->user()->following()->toggle($user->profile);
    }
}
