<?php

namespace App\Http\Controllers;

use App\Models\User;
use Intervention\Image\Facades\Image;
use \Illuminate\Support\Facades\Cache;

class ProfilesController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    function show(User $user) {
        $follows = auth()->user()->following->contains($user->id);

        $postCount = Cache::remember(
            'post.count.' . $user->id,
            now()->addSeconds(30),
            function() use($user) {
                return $user->posts()->count();
            }
        );
        $followersCount = Cache::remember(
            'followers.count.' . $user->id,
            now()->addSeconds(30),
            function() use($user) {
                return $user->profile->followers->count();
            }
        );
        $followingCount = Cache::remember(
            'following.count.' . $user->id,
            now()->addSeconds(30),
            function() use($user) {
                return $user->following->count();
            }
        );

        return view('profiles.show', compact(
                        'user', 'follows', 'postCount', 'followersCount', 'followingCount'
        ));
    }

    function edit(User $user) {
        $this->authorize('update', $user->profile);

        return view('profiles.edit', compact('user'));
    }

    function update(User $user) {
        $this->authorize('update', $user->profile);

        $data = request()->validate([
            'title' => 'required',
            'description' => 'required',
            'url' => ['required', 'url'],
            'image' => 'image'
        ]);

        if (request('image')) {
            $imagePath = request('image')->store('profile', 'public');
            $image = Image::make(public_path("storage/{$imagePath}"))->fit(1000, 1000);
            $image->save();

            $imageArray = ['image' => $imagePath];
        }

        auth()->user()->profile->update(array_merge($data, $imageArray ?? []));

        return redirect("/profile/{$user->id}");
    }

}
