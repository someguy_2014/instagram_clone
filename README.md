# instagram_clone using Laravel and vue.js

## Setup
- create `database/database.sqlite` file
- run `php artisan migrate`
- run `npm run dev`

## Run
- `php artisan serve`
